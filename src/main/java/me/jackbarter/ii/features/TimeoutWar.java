/*
 * Copyright (c) 2022 Jack Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package me.jackbarter.ii.features;

import me.jackbarter.ii.IntegralInspector;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.entities.TextChannel;

import java.awt.Color;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TimeoutWar {

	private static final Pattern minsPattern = Pattern.compile("\\d+");

	private static TextChannel timeoutWar;
	private static Message minutesMsg;
	private static int omarMins = 0;
	private static int andrewMins = 0;

	public static void init() {
		timeoutWar = IntegralInspector.getGuild().getTextChannelById("989692557262999573");
		retrieveMessage();
		retrieveStatistics();

		new Timer().schedule(new TimerTask() {

			@Override
			public void run() {
				performCheck();
			}

		}, 1000 * 60, 1000 * 60);
	}

	private static void retrieveMessage() {
		List<Message> history = timeoutWar.getHistory().retrievePast(1).complete();
		if(history.isEmpty()) {
			minutesMsg = timeoutWar.sendMessageEmbeds(buildEmbed()).complete();
		} else {
			minutesMsg = history.get(0);
		}
	}

	private static void retrieveStatistics() {
		List<MessageEmbed.Field> fields = minutesMsg.getEmbeds().get(0).getFields();

		Matcher omarMatcher = minsPattern.matcher(fields.get(0).getValue());
		if(!omarMatcher.find()) return;
		omarMins = Integer.parseInt(omarMatcher.group());

		Matcher andrewMatcher = minsPattern.matcher(fields.get(1).getValue());
		if(!andrewMatcher.find()) return;
		andrewMins = Integer.parseInt(andrewMatcher.group());
	}

	private static void performCheck() {
		omarMins = IntegralInspector.omar.isTimedOut() ? omarMins + 1 : omarMins;
		andrewMins = IntegralInspector.andrew.isTimedOut() ? andrewMins + 1 : andrewMins;

		minutesMsg.editMessageEmbeds(buildEmbed()).queue();
	}

	private static MessageEmbed buildEmbed() {
		return new EmbedBuilder()
				.setTitle("Total Minutes Timed Out")
				.setColor(Color.BLUE)
				.addField("Omar", omarMins + "m", false)
				.addField("Andrew", andrewMins + "m", false)
				.setFooter("Last Updated ")
				.setTimestamp(OffsetDateTime.now())
				.build();
	}

}
