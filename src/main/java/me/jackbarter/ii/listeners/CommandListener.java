/*
 * Copyright (c) 2022 Jack Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package me.jackbarter.ii.listeners;

import me.jackbarter.ii.IntegralInspector;
import me.jackbarter.ii.bell.Schedule;
import me.jackbarter.ii.bell.TheBell;
import me.jackbarter.ii.hw.ChecklistEmbed;
import me.jackbarter.ii.hw.ChecklistItem;
import me.jackbarter.ii.file.entries.hw.Homework;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.ChannelType;
import net.dv8tion.jda.api.entities.Emoji;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.dv8tion.jda.api.interactions.commands.OptionMapping;
import net.dv8tion.jda.api.interactions.components.Button;
import net.dv8tion.jda.api.interactions.components.ButtonStyle;

import javax.annotation.Nonnull;
import java.time.OffsetDateTime;
import java.time.temporal.ChronoUnit;

public class CommandListener extends ListenerAdapter {

	@Override
	public void onSlashCommand(@Nonnull SlashCommandEvent e) {
		if(!e.isFromGuild()) return;
		assert e.getGuild() != null;
		if(!e.getGuild().getId().equals(IntegralInspector.getGuild().getId())) return;
		if(e.getMember() == null) return;

		if(e.getName().equals("say")) {
			OptionMapping channelOpt = e.getOption("channel");
			OptionMapping msgOpt = e.getOption("message");

			// Validate args
			if(channelOpt == null || msgOpt == null) {
				e.reply(":x: Invalid arguments supplied! Please try again. :x:").queue();
				return;
			}
			if(!channelOpt.getChannelType().equals(ChannelType.TEXT)) {
				e.reply(":x: Please supply text channels only! :x:").queue();
				return;
			}

			MessageChannel channel = channelOpt.getAsMessageChannel();
			String msg = msgOpt.getAsString();

			channel.sendMessage(msg).queue();
			e.reply(":white_check_mark: Successfully sent the message into " + channel.getAsMention() + "! :white_check_mark:").queue();
		} else if(e.getName().equals("dm")) {
			OptionMapping userOpt = e.getOption("user");
			OptionMapping msgOpt = e.getOption("message");

			// Validate args
			if(userOpt == null || msgOpt == null) {
				e.reply(":x: Invalid arguments supplied! Please try again. :x:").queue();
				return;
			}

			User user = userOpt.getAsUser();
			String msg = msgOpt.getAsString();

			user.openPrivateChannel().queue(channel -> channel.sendMessage(msg).queue());
			e.reply(":white_check_mark: Successfully direct messaged " + user.getAsMention() + "! :white_check_mark:").queue();
		} else if(e.getName().equals("config")) {
			OptionMapping timeoutsOpt = e.getOption("timeouts");
			OptionMapping omarTimeoutsOpt = e.getOption("omar-timeouts");
			OptionMapping bhattiAdminsOpt = e.getOption("bhatti-admins");
			OptionMapping omarBurgersOpt = e.getOption("omar-burgers");

			// Validate args
			if(timeoutsOpt == null || omarTimeoutsOpt == null || bhattiAdminsOpt == null || omarBurgersOpt == null) {
				e.reply(":x: Invalid arguments supplied! Please try again. :x:").queue();
				return;
			}

			boolean timeouts = timeoutsOpt.getAsBoolean();
			boolean omarTimeouts = omarTimeoutsOpt.getAsBoolean();
			boolean bhattiAdmins = bhattiAdminsOpt.getAsBoolean();
			boolean omarBurgers = omarBurgersOpt.getAsBoolean();
			IntegralInspector.timeouts = timeouts;
			IntegralInspector.omarTimeouts = omarTimeouts;
			IntegralInspector.bhattiAdmins = bhattiAdmins;
			IntegralInspector.omarBurgers = omarBurgers;

			String response = "";
			response += timeouts ?
					":green_circle: Enabled timeouts for pinging the Head Integrator! :green_circle:\n" :
					":red_circle: Disabled timeouts for pinging the Head Integrator! :red_circle:\n";
			response += omarTimeouts ?
					":green_circle: Enabled timeouts if Omar pings the Head Integrator! :green_circle:\n" :
					":red_circle: Disabled timeouts if Omar pings the Head Integrator! :red_circle:\n";
			response += bhattiAdmins ?
					":green_circle: Enabled /bhatti for admins only! :green_circle:\n" :
					":red_circle: Disabled /bhatti for admins only! :red_circle:\n";
			response += omarBurgers ?
					":green_circle: Enabled Omar automatic burger reactions! :green_circle:" :
					":red_circle: Disabled Omar automatic burger reactions! :red_circle:";
			e.reply(response).queue();
		} else if(e.getName().contains("bhatti")) {
			if(IntegralInspector.bhattiAdmins) {
				boolean admin = e.getMember().hasPermission(Permission.ADMINISTRATOR);
				if(!admin) return;
			}

			if(e.getName().equals("bhatti")) {
				e.reply(addTimeout(IntegralInspector.omar, 10)).queue();
			} else if(e.getName().equals("superbhatti")) {
				e.reply(addTimeout(IntegralInspector.omar, 30)).queue();
			} else if(e.getName().equals("megabhatti")) {
				e.reply(addTimeout(IntegralInspector.omar, 60)).queue();
			} else if(e.getName().equals("gigabhatti")) {
				e.reply(addTimeout(IntegralInspector.omar, 180)).queue();
			} else if(e.getName().equals("unbhatti")) {
				IntegralInspector.omar.removeTimeout().queue();
				e.reply(":white_check_mark: Removed the timeout on Omar, if he had one! :white_check_mark:").queue();
			}
		} else if(e.getName().contains("adrewster")) {
			if(IntegralInspector.bhattiAdmins) {
				boolean admin = e.getMember().hasPermission(Permission.ADMINISTRATOR);
				if(!admin) return;
			}

			if(e.getName().equals("adrewster")) {
				e.reply(addTimeout(IntegralInspector.andrew, 10)).queue();
			} else if(e.getName().equals("unadrewster")) {
				IntegralInspector.andrew.removeTimeout().queue();
				e.reply(":white_check_mark: Removed the timeout on Adrewster, if he had one! :white_check_mark:").queue();
			}
		} else if(e.getName().equals("hw")) {
			String subcmd = e.getSubcommandName();
			if(subcmd == null) return;

			if(subcmd.equals("list")) {
				// Checklist
				MessageEmbed embed = new ChecklistEmbed(e.getMember(), e.getChannel()).builder().build();
				e.replyEmbeds(embed)
						.setEphemeral(true)
						.addActionRow(Button.of(ButtonStyle.PRIMARY, "share", "Share", Emoji.fromUnicode("U+1F4E8")))
						.queue();
			} else if(subcmd.equals("add")) {
				// Add hw
				OptionMapping classNameOpt = e.getOption("class");
				OptionMapping assignmentOpt = e.getOption("name");
				OptionMapping dueDateOpt = e.getOption("due-date");
				OptionMapping descOpt = e.getOption("desc");

				if(classNameOpt == null || assignmentOpt == null || dueDateOpt == null) {
					e.reply(":x: Invalid arguments supplied! Please try again. :x:").setEphemeral(true).queue();
					return;
				}

				String className = classNameOpt.getAsString();
				String assignment = assignmentOpt.getAsString();
				String dueDate = dueDateOpt.getAsString();
				String desc = descOpt == null ? "" : descOpt.getAsString();

				ChecklistItem item = new ChecklistItem(className, assignment, dueDate, desc);
				if(item.parseDueDate() == null) {
					e.reply(":x: Invalid due date specified! Please try again. :x:").setEphemeral(true).queue();
					return;
				}

				Homework.addItem(e.getMember(), item);
				e.reply(":white_check_mark: Successfully added the assignment to your homework checklist! :white_check_mark:").setEphemeral(true).queue();
			} else if(subcmd.equals("finish")) {
				// Finish hw
				if(Homework.getChecklist(e.getMember()).isEmpty()) {
					e.reply(":x: You're all caught up, so there's nothing to mark as finished! :x:").setEphemeral(true).queue();
					return;
				}

				MessageEmbed embed = new ChecklistEmbed(e.getMember(), e.getChannel()).finish();
				e.replyEmbeds(embed)
						.setEphemeral(true)
						.addActionRow(ButtonListener.getButtonsForFinishEmbed(embed))
						.queue();
			}
		} else if(e.getName().equals("schedule")) {
			OptionMapping internalNameOpt = e.getOption("internal-name");

			if(internalNameOpt == null) {
				e.reply(":x: Invalid arguments supplied! Please try again. :x:").queue();
				return;
			}

			String internalName = internalNameOpt.getAsString();

			if(!TheBell.SCHEDULES.containsKey(internalName)) {
				e.reply(":x: Invalid internal name for the schedule provided! Available options: `" +
						String.join(", ", TheBell.SCHEDULES.keySet()) + "` :x:").queue();
				return;
			}

			Schedule schedule = TheBell.SCHEDULES.get(internalName);
			TheBell.forceSchedule(schedule);
			e.reply(":white_check_mark: Set today's schedule to \"" + schedule.getDisplayName() + "\" forcibly! :white_check_mark:").queue();
		} else if(e.getName().equals("max-tokens")) {
			OptionMapping internalMaxTokens = e.getOption("max-tokens");

			if(internalMaxTokens == null) {
				e.reply(":x: Invalid arguments supplied! Please try again. :x:").queue();
				return;
			}

			MessageListener.maxTokens = (int) internalMaxTokens.getAsLong();

			e.reply(":white_check_mark: Successfully set the max AI tokens to **" + MessageListener.maxTokens + "**! :white_check_mark:").queue();
		}
	}

	private static String addTimeout(Member member, int mins) {
		OffsetDateTime timeoutEnd = member.isTimedOut() ?
				member.getTimeOutEnd().plus(mins, ChronoUnit.MINUTES) :
				OffsetDateTime.now().plus(mins, ChronoUnit.MINUTES);
		member.timeoutUntil(timeoutEnd).queue();

		long totalSecs = timeoutEnd.minus(System.currentTimeMillis(), ChronoUnit.MILLIS).toEpochSecond();
		long minsLeft = totalSecs / 60;
		long secsLeft = totalSecs - (minsLeft * 60);
		return ":white_check_mark: " + member.getEffectiveName() + " has been timed out, their timeout ends in **" +
				minsLeft + "m " + secsLeft + "s**! :white_check_mark:";
	}

}
