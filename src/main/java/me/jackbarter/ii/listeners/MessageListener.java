/*
 * Copyright (c) 2022 Jack Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package me.jackbarter.ii.listeners;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import me.jackbarter.ii.IntegralInspector;
import me.jackbarter.ii.file.Database;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.ChannelType;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.jetbrains.annotations.NotNull;

import javax.net.ssl.HttpsURLConnection;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.time.Duration;

public class MessageListener extends ListenerAdapter {

	public static int maxTokens = 512;

	@Override
	public void onMessageReceived(@NotNull MessageReceivedEvent e) {
		if(e.getAuthor().isBot() || e.getAuthor().isSystem()) return;
		if(!e.isFromGuild()) {
			// Incoming DM
			if(!e.isFromType(ChannelType.PRIVATE)) return;

			IntegralInspector.dmLogs.sendMessage("**Incoming DM from " + e.getAuthor().getAsMention() + ":**\n" +
					">>> " + e.getMessage().getContentDisplay()).queue();
		} else {
			// Message scraper
			if(!e.getGuild().getId().equals(IntegralInspector.getGuild().getId())) return;
			if(!e.isFromType(ChannelType.TEXT)) return;

			Member member = e.getMember();
			Message msg = e.getMessage();
			TextChannel channel = e.getTextChannel();
			if(member == null) return;

			if(msg.getContentRaw().contains("<@222027482990116875>")) {
				if(member.hasPermission(Permission.ADMINISTRATOR)) return;
				if(!IntegralInspector.timeouts && !member.getId().equals(IntegralInspector.omar.getId())) return;
				if(!IntegralInspector.omarTimeouts && member.getId().equals(IntegralInspector.omar.getId())) return;

				msg.reply(":x: You have been timed out for **30** minutes for pinging the Head Integrator! :x:").queue();
				member.timeoutFor(Duration.ofMinutes(30)).queue();
			}

			if(msg.getContentRaw().toLowerCase().contains("mulligan")) {
				if(member.hasPermission(Permission.ADMINISTRATOR)) return;
				msg.reply(":x: You have been timed out for **10** minutes for saying \"mulligan\"! :x:").queue();
				member.timeoutFor(Duration.ofMinutes(10)).queue();
			}

			if(member.getId().equals(IntegralInspector.omar.getId()) && IntegralInspector.omarBurgers) {
				msg.addReaction("U+1F354").queue();
			}

			if(channel.getId().equals(IntegralInspector.chatGpt.getId())) {
				Message reply = msg.reply(":thinking: Let me think about that...").complete();

				new Thread(() -> {
					try {
						String response = gptCompletion(msg.getContentRaw() + ".");
						reply.editMessage(":white_check_mark: " + member.getAsMention() + ":\n" + response).queue();
					} catch (IOException ex) {
						reply.editMessage(":warning: " + member.getAsMention() + ": I am having trouble executing your request. Try again later!").queue();
					}
				}).start();
			}
		}
	}

	private static String gptCompletion(String prompt) throws IOException {
		URL url = new URL("https://api.openai.com/v1/completions");
		HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
		conn.setRequestMethod("POST");
		conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
		conn.setRequestProperty("Authorization", "Bearer " + Database.getGPT().getAPIKey());
		conn.setRequestProperty("OpenAI-Organization", Database.getGPT().getOrganizationId());
		conn.setDoOutput(true);
		conn.connect();

		OutputStream out = conn.getOutputStream();
		out.write(("{" +
				"\"model\": \"text-davinci-003\"," +
				"\"prompt\": \"" + prompt + "\"," +
				"\"max_tokens\": " + maxTokens + "," +
				"\"temperature\": 1," +
				"\"n\": 1 }").getBytes());
		out.flush();
		out.close();

		InputStream in = conn.getInputStream();
		JsonObject responseData = JsonParser.parseString(new String(in.readAllBytes())).getAsJsonObject();
		String response = responseData.getAsJsonArray("choices").get(0).getAsJsonObject().getAsJsonPrimitive("text").getAsString().trim();
		return response.length() > maxTokens ? response.substring(0, maxTokens + 1) : response;
	}

}
