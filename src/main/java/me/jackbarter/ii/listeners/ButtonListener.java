/*
 * Copyright (c) 2022 Jack Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package me.jackbarter.ii.listeners;

import me.jackbarter.ii.hw.ChecklistEmbed;
import me.jackbarter.ii.hw.ChecklistItem;
import me.jackbarter.ii.file.entries.hw.Homework;
import net.dv8tion.jda.api.entities.Emoji;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.events.interaction.ButtonClickEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.dv8tion.jda.api.interactions.components.Button;
import net.dv8tion.jda.api.interactions.components.ButtonStyle;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.List;

public class ButtonListener extends ListenerAdapter {

	@Override
	public void onButtonClick(@Nonnull ButtonClickEvent e) {
		if(e.getMember() == null) return;
		if(e.getButton() == null) return;
		if(e.getButton().getId() == null) return;

		if(e.getButton().getId().equals("share")) {
			new ChecklistEmbed(e.getMember(), e.getChannel()).publish();
			e.reply(":white_check_mark: Shared your homework checklist to the channel! :white_check_mark:").setEphemeral(true).queue();
		} else if(e.getButton().getId().startsWith("remove")) {
			int index = Integer.parseInt(e.getButton().getLabel().substring(1, e.getButton().getLabel().length() - 1)) - 1;
			ChecklistItem finishedItem = Homework.getChecklist(e.getMember()).get(index);
			Homework.removeItem(e.getMember(), finishedItem.getAssignment());

			e.editComponents().setActionRows().queue();
			e.getMember().getUser()
					.openPrivateChannel()
					.queue(c -> c.sendMessage(":white_check_mark: Marked the assignment as finished! :white_check_mark:").queue());
		}
	}

	public static List<Button> getButtonsForFinishEmbed(MessageEmbed embed) {
		List<Button> buttons = new ArrayList<>();
		for(int i = 0; i < embed.getFields().size(); i++) {
			if(i % 2 == 1) continue;
			MessageEmbed.Field field = embed.getFields().get(i);
			if(field.getName() == null) continue;
			String number = field.getName().substring(0, field.getName().indexOf("]") + 1);
			buttons.add(Button.of(ButtonStyle.DANGER, "remove" + i, number, Emoji.fromUnicode("U+1F5D1")));
		}
		return buttons;
	}

}
