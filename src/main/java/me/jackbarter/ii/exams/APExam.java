/*
 * Copyright (c) 2022 Jack Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package me.jackbarter.ii.exams;

import me.jackbarter.ii.bell.TheBell;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Date;

public enum APExam {

	US_GOV("United States Government and Politics", 1, ExamTime.MORNING),
	CHEM("Chemistry", 1, ExamTime.AFTERNOON_12),
	SPANISH_LIT("Spanish Literature and Culture", 1, ExamTime.AFTERNOON_12),
	CHINESE("Chinese Language and Culture", 2, ExamTime.MORNING),
	ENVIRONMENTAL("Environmental Science", 2, ExamTime.MORNING),
	PSYCH("Psychology", 2, ExamTime.AFTERNOON_12),
	LIT("English Literature and Composition", 3, ExamTime.MORNING),
	COMPARATIVE_GOV("Comparative Government and Politics", 3, ExamTime.AFTERNOON_12),
	CS_A("Computer Science A", 3, ExamTime.AFTERNOON_12),
	GEOGRAPHY("Human Geography", 4, ExamTime.MORNING),
	MACRO("Macroeconomics", 4, ExamTime.MORNING),
	SEMINAR("Seminar", 4, ExamTime.AFTERNOON_12),
	STAT("Statistics", 4, ExamTime.AFTERNOON_12),
	EURO("European History", 5, ExamTime.MORNING),
	US_HISTORY("United States History", 5, ExamTime.MORNING),
	ART("Art History", 5, ExamTime.AFTERNOON_12),
	MICRO("Microeconomics", 5, ExamTime.AFTERNOON_12),
	CALC_AB("Calculus AB", 8, ExamTime.MORNING),
	CALC_BC("Calculus BC", 8, ExamTime.MORNING),
	CS_P("Computer Science Principles", 8, ExamTime.AFTERNOON_12),
	ITALIAN("Italian Language and Culture", 8, ExamTime.AFTERNOON_12),
	LANG("English Language and Composition", 9, ExamTime.MORNING),
	JAPANESE("Japanese Language and Culture", 9, ExamTime.MORNING),
	PHYSICS_C("Physics C: Mechanics", 9, ExamTime.AFTERNOON_12),
	PHYSICS_C_ELECTRIC("Physics C: Electricity and Magnetism", 9, ExamTime.AFTERNOON_2),
	SPANISH_LANG("Spanish Language and Culture", 10, ExamTime.MORNING),
	BIO("Biology", 10, ExamTime.AFTERNOON_12),
	FRENCH("French Language and Culture", 11, ExamTime.MORNING),
	MODERN_WORLD("World History: Modern", 11, ExamTime.MORNING),
	PHYSICS_I("Physics I: Algebra-Based", 11, ExamTime.AFTERNOON_12),
	GERMAN("German Language and Culture", 12, ExamTime.MORNING),
	MUSIC("Music Theory", 12, ExamTime.AFTERNOON_12),
	LATIN("Latin", 12, ExamTime.AFTERNOON_12),
	PHYSICS_II("Physics II: Algebra-Based", 12, ExamTime.AFTERNOON_12);

	private String displayName;
	private Date date;
	private ExamTime time;

	APExam(String displayName, int dayInMay, ExamTime time) {
		try {
			this.displayName = displayName;
			this.date = new SimpleDateFormat("MM/dd/yy").parse("05/" + dayInMay + "/23");
			this.time = time;
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	public String getCountdown() {
		ZoneId zoneId = ZoneId.of("America/New_York");
		ZonedDateTime dateTime = ZonedDateTime.ofInstant(this.date.toInstant().plus(this.time.getHourInDay(), ChronoUnit.HOURS), zoneId);
		Duration duration = Duration.between(TheBell.getDateTime(), dateTime);
		return String.format("%dd %dh %dm", duration.toDaysPart(), duration.toHoursPart(), duration.toMinutesPart());
	}

	public String getDisplayName() {
		return displayName;
	}

	public ExamTime getTime() {
		return time;
	}

}