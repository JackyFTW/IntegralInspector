/*
 * Copyright (c) 2022 Jack Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package me.jackbarter.ii.exams;

import me.jackbarter.ii.IntegralInspector;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.entities.TextChannel;

import java.awt.Color;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class Exams {

	private static TextChannel apExams;

	public static void init() {
		apExams = IntegralInspector.getGuild().getTextChannelById("1015047572672499773");

		new Timer().schedule(new TimerTask() {

			@Override
			public void run() {
				List<Message> history = apExams.getHistory().retrievePast(1).complete();
				if(history.isEmpty()) {
					apExams.sendMessageEmbeds(buildEmbeds()).queue();
				} else {
					history.get(0).editMessageEmbeds(buildEmbeds()).queue();
				}
			}

		}, 0, 1000 * 60 * 30);
	}

	private static List<MessageEmbed> buildEmbeds() {
		List<MessageEmbed> embeds = new ArrayList<>();

		EmbedBuilder builder = new EmbedBuilder()
				.setColor(Color.BLUE)
				.setFooter("Last Updated ")
				.setTimestamp(OffsetDateTime.now());

		for(ExamTime time : ExamTime.values()) {
			builder.setTitle(time.getDisplayName() + " Exams:");
			for(APExam exam : APExam.values()) {
				if(!exam.getTime().equals(time)) continue;

				builder.addField(exam.getDisplayName(), exam.getCountdown(), false);
			}
			embeds.add(builder.build());
			builder.clearFields();
		}

		return embeds;
	}

}
