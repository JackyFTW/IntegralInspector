/*
 * Copyright (c) 2022 Jack Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package me.jackbarter.ii.file.entries.hw;

import me.jackbarter.ii.file.Database;
import me.jackbarter.ii.hw.ChecklistItem;
import net.dv8tion.jda.api.entities.Member;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class Homework {

	private final HashMap<String, List<ChecklistItem>> data = new HashMap<>();

	public static void addItem(Member member, ChecklistItem item) {
		if(Database.getHomework().data.containsKey(member.getId())) {
			List<ChecklistItem> checklist = getChecklist(member);
			checklist.add(item);
			Database.getHomework().data.replace(member.getId(), checklist);
		} else {
			Database.getHomework().data.put(member.getId(), Collections.singletonList(item));
		}
	}

	public static List<ChecklistItem> getChecklist(Member member) {
		Database.getHomework().data.putIfAbsent(member.getId(), new ArrayList<>());
		List<ChecklistItem> items = new ArrayList<>(Database.getHomework().data.get(member.getId()));
		items.removeIf(item -> {
			if(item.isExpired()) {
				member.getUser().openPrivateChannel().queue(channel ->
								channel.sendMessage(":exclamation: Your homework assignment \"" + item.getAssignment() + "\" has expired! :exclamation:").queue());
				return true;
			} else {
				return false;
			}
		});
		Database.getHomework().data.replace(member.getId(), items);
		Collections.sort(items);
		return items;
	}

	public static void removeItem(Member member, String assignment) {
		List<ChecklistItem> checklist = getChecklist(member);
		checklist.removeIf(item -> item.getAssignment().equals(assignment));
		Database.getHomework().data.replace(member.getId(), checklist);
	}

}
