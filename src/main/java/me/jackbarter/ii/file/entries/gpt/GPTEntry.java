/*
 * Copyright (c) 2022 Jack Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package me.jackbarter.ii.file.entries.gpt;

import com.google.gson.Gson;
import me.jackbarter.ii.file.Database;
import me.jackbarter.ii.file.FileEntry;
import me.jackbarter.ii.file.entries.config.Config;

public class GPTEntry extends FileEntry<GPT> {

	private GPT value;

	public GPTEntry() {
		super("gpt", GPT::new);
	}

	@Override
	public GPT getValue() {
		if(value == null) {
			value = new Gson().fromJson(Database.getDatabase().getAsJsonObject(getKey()), GPT.class);
		}

		return value;
	}

}
