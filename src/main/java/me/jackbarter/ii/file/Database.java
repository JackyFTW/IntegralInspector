/*
 * Copyright (c) 2022 Jack Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package me.jackbarter.ii.file;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import me.jackbarter.ii.file.entries.config.Config;
import me.jackbarter.ii.file.entries.config.ConfigEntry;
import me.jackbarter.ii.file.entries.gpt.GPT;
import me.jackbarter.ii.file.entries.gpt.GPTEntry;
import me.jackbarter.ii.file.entries.hw.HomeworkEntry;
import me.jackbarter.ii.file.entries.hw.Homework;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class Database {

	private static final List<FileEntry<?>> fileEntries = new ArrayList<>();

	private static final File dbFile = new File("database.json");
	private static JsonObject database;

	@SuppressWarnings("ResultOfMethodCallIgnored")
	public static void init() throws IOException {
		fileEntries.add(new ConfigEntry());
		fileEntries.add(new GPTEntry());
		fileEntries.add(new HomeworkEntry());

		// Create default variables
		if(dbFile.exists()) {
			database = JsonParser.parseReader(new FileReader(dbFile)).getAsJsonObject();
		} else {
			dbFile.createNewFile();
			database = new JsonObject();
		}

		// Add default values
		for(FileEntry<?> entry : fileEntries) {
			if(!database.has(entry.getKey())) {
				database.add(entry.getKey(), new Gson().toJsonTree(entry.getDefaultValue()));
			}
		}

		// Auto-save
		new Timer().scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				save();
			}
		}, 0, 1000 * 60 * 10);
	}

	public static void save() {
		try {
			FileWriter fw = new FileWriter(dbFile);

			// Save any changes to entry objects
			for(FileEntry<?> entry : fileEntries) {
				database.add(entry.getKey(), new Gson().toJsonTree(entry.getValue()));
			}

			fw.write(new GsonBuilder().setPrettyPrinting().create().toJson(database));
			fw.flush();
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static Config getConfig() {
		return (Config) getEntry("config").getValue();
	}

	public static GPT getGPT() {
		return (GPT) getEntry("gpt").getValue();
	}

	public static Homework getHomework() {
		return (Homework) getEntry("hw").getValue();
	}

	private static FileEntry<?> getEntry(String key) {
		return fileEntries.stream()
				.filter(e -> e.getKey().equalsIgnoreCase(key))
				.findFirst()
				.orElse(null);
	}

	public static JsonObject getDatabase() {
		return database;
	}

}
