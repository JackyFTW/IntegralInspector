/*
 * Copyright (c) 2022 Jack Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package me.jackbarter.ii;

import me.jackbarter.ii.bell.TheBell;
import me.jackbarter.ii.exams.Exams;
import me.jackbarter.ii.features.TimeoutWar;
import me.jackbarter.ii.file.Database;
import me.jackbarter.ii.listeners.ButtonListener;
import me.jackbarter.ii.listeners.CommandListener;
import me.jackbarter.ii.listeners.MessageListener;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.entities.Activity;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.build.CommandData;
import net.dv8tion.jda.api.interactions.commands.build.SubcommandData;
import net.dv8tion.jda.api.requests.GatewayIntent;
import net.dv8tion.jda.api.utils.MemberCachePolicy;
import net.dv8tion.jda.api.utils.cache.CacheFlag;

import javax.security.auth.login.LoginException;
import java.io.IOException;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;
import java.util.logging.Level;

public class IntegralInspector {

	private static JDA jda;

	public static Member adam;
	public static Member omar;
	public static Member andrew;
	public static TextChannel dmLogs;
	public static TextChannel chatGpt;

	public static boolean timeouts = false;
	public static boolean omarTimeouts = false;
	public static boolean bhattiAdmins = false;
	public static boolean omarBurgers = false;

	public static void main(String[] args) throws LoginException, InterruptedException, IOException {
		// Disable stupid fucking warnings
		System.setErr(new PrintStream(System.err) {
			@Override
			public void println(String s) {
				if(!s.startsWith("SLF4J")) super.println(s);
			}
		});

		// Shutdown hook
		Runtime.getRuntime().addShutdownHook(new Thread(IntegralInspector::shutdown));

		// Listen for stop
		new Thread(() -> {
			Scanner in = new Scanner(System.in);
			while(in.hasNextLine()) {
				if(in.nextLine().equalsIgnoreCase("stop")) {
					System.exit(0);
					break;
				}
			}
		}).start();

		// Start
		startBot();
	}

	private static void startBot() throws LoginException, InterruptedException, IOException {
		log(Level.INFO, "Logging in...");

		Database.init();
		jda = JDABuilder.createDefault(Database.getConfig().getToken(),
						GatewayIntent.DIRECT_MESSAGES,
						GatewayIntent.GUILD_MESSAGES,
						GatewayIntent.GUILD_MEMBERS)
				.setMemberCachePolicy(MemberCachePolicy.ALL)
				.disableCache(CacheFlag.VOICE_STATE, CacheFlag.EMOTE)
				.setActivity(Activity.playing("with a calculus textbook"))
				.addEventListeners(new CommandListener(), new MessageListener(), new ButtonListener())
				.build()
				.awaitReady();

		adam = getGuild().retrieveMemberById("222027482990116875").complete();
		omar = getGuild().retrieveMemberById("657367374164393994").complete();
		andrew = getGuild().retrieveMemberById("426522586592182275").complete();
		dmLogs = getGuild().getTextChannelById("975873539569745990");
		chatGpt = getGuild().getTextChannelById("1052679211271327797");

		registerCommands();
		TheBell.init();
		Exams.init();
		TimeoutWar.init();

		log(Level.INFO, "Logged in successfully!");
	}

	private static void registerCommands() {
		getGuild().upsertCommand(new CommandData("say", "Sends a message in a text channel as the bot.")
						.addOption(OptionType.CHANNEL, "channel", "The target channel for the message.", true)
						.addOption(OptionType.STRING, "message", "The message to be sent in the channel.", true))
				.queue();
		getGuild().upsertCommand(new CommandData("dm", "Sends a guild member a direct message as the bot.")
						.addOption(OptionType.USER, "user", "The user to direct message.", true)
						.addOption(OptionType.STRING, "message", "The message to be sent in direct message.", true))
				.queue();
		getGuild().upsertCommand(new CommandData("config", "Manages the current configuration of the server and its options.")
						.addOption(OptionType.BOOLEAN, "timeouts", "Whether timeouts are enabled or disabled for pinging Adam.", true)
						.addOption(OptionType.BOOLEAN, "omar-timeouts", "Whether timeouts are enabled or disabled for Omar pinging Adam.", true)
						.addOption(OptionType.BOOLEAN, "bhatti-admins", "Whether admins are the only people who can timeout Omar using /bhatti.", true)
						.addOption(OptionType.BOOLEAN, "omar-burgers", "Whether all messages from Omar come with a burger reaction or not.", true))
				.queue();
		getGuild().upsertCommand(new CommandData("hw", "Manages your homework. Used for adding and removing assignments.")
						.addSubcommands(new SubcommandData("list", "Shows your homework checklist."))
						.addSubcommands(new SubcommandData("add", "Adds homework to be due.")
								.addOption(OptionType.STRING, "class", "The class that the homework assignment belongs to.", true)
								.addOption(OptionType.STRING, "name", "The name of the homework assignment.", true)
								.addOption(OptionType.STRING, "due-date", "The date that the homework assignment is due.", true)
								.addOption(OptionType.STRING, "desc", "The description of the homework assignment."))
						.addSubcommands(new SubcommandData("finish", "Marks homework assignments as finished.")))
				.queue();
		getGuild().upsertCommand(new CommandData("schedule", "Set the schedule for the school day forcibly.")
						.addOption(OptionType.STRING, "internal-name", "The internal name of the schedule to select.", true))
				.queue();
		getGuild().upsertCommand(new CommandData("max-tokens", "Sets the max_tokens for the bot to use for OpenAI API calls.")
						.addOption(OptionType.INTEGER, "max-tokens", "The value to set the max_tokens to (0 < max_tokens <= 2048).", true))
				.queue();

		getGuild().upsertCommand(new CommandData("bhatti", "Times out Omar for approximately 10 minutes.")).queue();
		getGuild().upsertCommand(new CommandData("superbhatti", "Times out Omar for approximately 30 minutes.")).queue();
		getGuild().upsertCommand(new CommandData("megabhatti", "Times out Omar for approximately 60 minutes.")).queue();
		getGuild().upsertCommand(new CommandData("gigabhatti", "Times out Omar for approximately 3 hours.")).queue();
		getGuild().upsertCommand(new CommandData("unbhatti", "Removes Omar's timeout, if he has one.")).queue();
		getGuild().upsertCommand(new CommandData("adrewster", "Times out Adrewster for approximately 10 minutes.")).queue();
		getGuild().upsertCommand(new CommandData("unadrewster", "Removes Adrewster's timeout, if he has one.")).queue();
	}

	private static void shutdown() {
		log(Level.SEVERE, "Shutting down...");
		jda.shutdown();
		Database.save();
		log(Level.SEVERE, "Shutdown complete successfully!");
	}

	public static void log(Level lvl, String msg) {
		SimpleDateFormat format = new SimpleDateFormat("'[" + lvl.getName() + "]' '['MM-dd HH:mm:ss.S']' ");
		String prefix = format.format(new Date());
		System.out.println(prefix + msg);
	}

	public static Guild getGuild() {
		return jda.getGuildById("971182890215936040");
	}

}
