/*
 * Copyright (c) 2022 Jack Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package me.jackbarter.ii.hw;

import me.jackbarter.ii.file.entries.hw.Homework;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.MessageEmbed;

import java.awt.Color;
import java.util.List;

public class ChecklistEmbed {

	private final Member member;
	private final MessageChannel channel;

	public ChecklistEmbed(Member member, MessageChannel channel) {
		this.member = member;
		this.channel = channel;
	}

	public EmbedBuilder builder() {
		List<ChecklistItem> items = Homework.getChecklist(this.member);

		EmbedBuilder builder = new EmbedBuilder()
				.setTitle("Your Homework Checklist:")
				.setColor(Color.BLUE)
				.setDescription(items.isEmpty() ? "You're all caught up! :confetti_ball:" : "")
				.setFooter(ChecklistItem.Status.NORMAL.getUnicode() + " = Not due soon | "
						+ ChecklistItem.Status.DUE.getUnicode() + " = Due within 24h | "
						+ ChecklistItem.Status.URGENT.getUnicode() + " = Due within 12h", this.member.getEffectiveAvatarUrl());
		for(int i = 0; i < items.size(); i++) {
			ChecklistItem item = items.get(i);
			String name = "[" + (i + 1) + "] " + item.getStatus().getUnicode() + " __" + item.getAssignment() + "__ *(" + item.getClassName() + ")*";
			builder.addField(name, "<" + item.getDueDate() + "> " + item.getDesc(), false);

			if(i != items.size() - 1) builder.addField("\u200b", "\u200b", false);
		}
		return builder;
	}

	public MessageEmbed finish() {
		return builder().setTitle("Mark Finished Assignment:").build();
	}

	public void publish() {
		EmbedBuilder builder = builder();
		MessageEmbed embed = builder.setTitle(this.member.getEffectiveName() + "'s Homework Checklist:")
				.setDescription(builder.getFields().size() == 0 ? "They're all caught up! :confetti_ball:" : "")
				.build();
		this.channel.sendMessageEmbeds(embed).queue();
	}

}
