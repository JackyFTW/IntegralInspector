/*
 * Copyright (c) 2022 Jack Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package me.jackbarter.ii.hw;

import org.jetbrains.annotations.NotNull;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;

public class ChecklistItem implements Comparable<ChecklistItem> {

	public enum Status {

		NORMAL("\uD83D\uDD18"),
		DUE("\u26A0\uFE0F"),
		URGENT("\u203C\uFE0F");

		private final String unicode;

		Status(String unicode) {
			this.unicode = unicode;
		}

		public String getUnicode() {
			return unicode;
		}
	}

	private static final String DUE_DATE_FORMAT = "MM/dd/yy";

	private final String className;
	private final String assignment;
	private final String dueDate;
	private final String desc;

	public ChecklistItem(String className, String assignment, String dueDate, String desc) {
		this.className = className;
		this.assignment = assignment;
		this.dueDate = dueDate;
		this.desc = desc;
	}

	public Date parseDueDate() {
		try {
			SimpleDateFormat format = new SimpleDateFormat(DUE_DATE_FORMAT);
			Date date = format.parse(this.dueDate);

			return date.after(new Date()) ? date : null;
		} catch (ParseException e) {
			return null;
		}
	}

	public Status getStatus() {
		if(parseDueDate().toInstant().minus(12, ChronoUnit.HOURS).isBefore(Instant.now())) {
			return Status.URGENT;
		} else if(parseDueDate().toInstant().minus(1, ChronoUnit.DAYS).isBefore(Instant.now())) {
			return Status.DUE;
		} else {
			return Status.NORMAL;
		}
	}

	public boolean isExpired() {
		try {
			return !new SimpleDateFormat(DUE_DATE_FORMAT).parse(this.dueDate).after(new Date());
		} catch (ParseException e) {
			return true;
		}
	}

	public String getClassName() {
		return className;
	}

	public String getAssignment() {
		return this.assignment;
	}

	public String getDueDate() {
		return dueDate;
	}

	public String getDesc() {
		return desc;
	}

	@Override
	public int compareTo(@NotNull ChecklistItem item) {
		Date date = parseDueDate();
		Date otherDate = item.parseDueDate();

		if(date.before(otherDate)) {
			return -1;
		} else if(date.equals(otherDate)) {
			return 0;
		} else {
			return 1;
		}
	}

}
