/*
 * Copyright (c) 2022 Jack Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package me.jackbarter.ii.bell;

import java.awt.Color;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;

public abstract class ScheduleEntry {

	private final String displayName;
	private final String startTime;
	private final String endTime;
	private final String thumbnailUrl;

	public ScheduleEntry(String displayName, String startTime, String endTime, String thumbnailUrl) {
		this.displayName = displayName;
		this.startTime = startTime;
		this.endTime = endTime;
		this.thumbnailUrl = thumbnailUrl;
	}

	public abstract Color getStatusColor();

	public String getDisplayName() {
		return this.displayName;
	}

	public ZonedDateTime getStartDateTime() {
		try {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(new SimpleDateFormat("hh:mm a").parse(this.startTime));
			return TheBell.getDateTime()
					.truncatedTo(ChronoUnit.DAYS)
					.plusHours(calendar.get(Calendar.HOUR_OF_DAY))
					.plusMinutes(calendar.get(Calendar.MINUTE))
					.plusSeconds(0);
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
	}

	public ZonedDateTime getEndDateTime() {
		try {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(new SimpleDateFormat("hh:mm a").parse(this.endTime));
			return TheBell.getDateTime()
					.truncatedTo(ChronoUnit.DAYS)
					.plusHours(calendar.get(Calendar.HOUR_OF_DAY))
					.plusMinutes(calendar.get(Calendar.MINUTE))
					.minusMinutes(1)
					.plusSeconds(59);
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
	}

	public long getMinsUntil() {
		return Duration.between(TheBell.getDateTime(), getStartDateTime()).toMinutes();
	}

	public String getStartTime() {
		return this.startTime;
	}

	public String getEndTime() {
		return this.endTime;
	}

	public String getThumbnailUrl() {
		return this.thumbnailUrl;
	}

	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof ScheduleEntry)) return false;
		ScheduleEntry other = (ScheduleEntry) obj;

		return this.displayName.equals(other.getDisplayName()) &&
				this.startTime.equals(other.getStartTime()) &&
				this.endTime.equals(other.getEndTime()) &&
				this.thumbnailUrl.equals(other.getThumbnailUrl());
	}

}
