/*
 * Copyright (c) 2022 Jack Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package me.jackbarter.ii.bell;

import me.jackbarter.ii.IntegralInspector;
import me.jackbarter.ii.bell.schedules.AssemblySched;
import me.jackbarter.ii.bell.schedules.NoSchoolSched;
import me.jackbarter.ii.bell.schedules.NormalSched;
import me.jackbarter.ii.bell.schedules.NormalWednesSched;
import me.jackbarter.ii.bell.schedules.StartOfYearSched;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.entities.TextChannel;

import java.awt.Color;
import java.time.Duration;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class TheBell {

	public static final HashMap<String, Schedule> SCHEDULES = new HashMap<>();

	private static TextChannel theBell;
	private static Schedule schedule;
	private static Schedule forcedSchedule;
	private static int lastDayOfWeek;

	public static void init() {
		SCHEDULES.put("start-of-year", new StartOfYearSched());
		SCHEDULES.put("no-school", new NoSchoolSched());
		SCHEDULES.put("normal", new NormalSched());
		SCHEDULES.put("normal-wednes", new NormalWednesSched());
		SCHEDULES.put("assembly", new AssemblySched());

		theBell = IntegralInspector.getGuild().getTextChannelById("1016483719248351293");

		// Schedule Embed
		updateSchedule(); // must be completed before other timer runs
		new Timer().schedule(new TimerTask() {

			@Override
			public void run() {
				updateSchedule();
			}

		}, 1000 * 60 * 60 * 3, 1000 * 60 * 60 * 3); // every 3 hours

		// Status Embed
		new Timer().schedule(new TimerTask() {

			@Override
			public void run() {
				List<Message> history = theBell.getHistory().retrievePast(2).complete();
				if(history.isEmpty()) {
					theBell.sendMessageEmbeds(buildStatusEmbed()).queue();
				} else {
					history.get(0).editMessageEmbeds(buildStatusEmbed()).queue();
				}
			}

		}, 0, 1000 * 15);
	}

	private static void updateSchedule() {
		schedule = null;

		if(forcedSchedule != null) {
			if(lastDayOfWeek != getDayOfWeek()) {
				// Next day, go back to programmed schedule
				forcedSchedule = null;
				updateSchedule();
				return;
			}

			lastDayOfWeek = getDayOfWeek();
			schedule = forcedSchedule;
		} else {
			lastDayOfWeek = getDayOfWeek();
			if(lastDayOfWeek == 3) {
				schedule = SCHEDULES.get("normal-wednes");
			} else if(lastDayOfWeek <= 5) {
				schedule = SCHEDULES.get("normal");
			} else {
				schedule = SCHEDULES.get("no-school");
			}
		}

		List<Message> history = theBell.getHistory().retrievePast(2).complete();
		if(history.size() != 2) {
			theBell.sendMessageEmbeds(schedule == null ? Schedule.buildNullEmbed() : schedule.buildEmbed()).queue();
		} else {
			history.get(1).editMessageEmbeds(schedule == null ? Schedule.buildNullEmbed() : schedule.buildEmbed()).queue();
		}
	}

	private static MessageEmbed buildStatusEmbed() {
		ScheduleEntry entry = schedule.getCurrentEntry();

		EmbedBuilder builder = new EmbedBuilder()
				.setTitle("Status (" + DateTimeFormatter.ofPattern("hh:mm a").format(getDateTime()) + ")")
				.setFooter("Last Updated")
				.setTimestamp(Instant.now());

		if(entry != null) {
			// School in session
			ScheduleEntry nextEntry = schedule.getNextEntry();

			builder.setColor(entry.getStatusColor())
					.setThumbnail(entry.getThumbnailUrl().equals("") ? null : entry.getThumbnailUrl())
					.addField("Current Period", entry.getDisplayName(), true)
					.addField("Time Range", entry.getStartTime() + " - " + entry.getEndTime(), true);

			if(nextEntry != null) {
				// Still another entry
				builder.addField(nextEntry.getDisplayName(), "In " + nextEntry.getMinsUntil() + " minutes", true);
			} else {
				// Next is dismissal
				long minsUntilDismissal = Duration.between(getDateTime(), entry.getEndDateTime()).toMinutes();
				builder.addField("Dismissal", "In " + minsUntilDismissal + " minutes", true);
			}
		} else {
			// No school
			builder.setColor(Color.RED)
					.setDescription("Check back here when the school day starts!");
		}

		return builder.build();
	}

	public static void forceSchedule(Schedule schedule) {
		forcedSchedule = schedule;
		updateSchedule();
	}

	public static ZonedDateTime getDateTime() {
		return ZonedDateTime.now(ZoneId.of("America/New_York"));
	}

	private static int getDayOfWeek() {
		return getDateTime().getDayOfWeek().getValue();
	}

}
