/*
 * Copyright (c) 2022 Jack Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package me.jackbarter.ii.bell.entries;

import me.jackbarter.ii.bell.ScheduleEntry;

import java.awt.Color;

public class PeriodEntry extends ScheduleEntry {

	public PeriodEntry(String displayName, String startTime, String endTime, String thumbnailUrl) {
		super(displayName, startTime, endTime, thumbnailUrl);
	}

	@Override
	public Color getStatusColor() {
		return Color.GREEN;
	}

}
