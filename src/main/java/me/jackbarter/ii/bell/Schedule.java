/*
 * Copyright (c) 2022 Jack Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package me.jackbarter.ii.bell;

import me.jackbarter.ii.bell.entries.PassingTimeEntry;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.MessageEmbed;

import java.awt.Color;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;

public abstract class Schedule {

	private final String displayName;

	public Schedule(String displayName) {
		this.displayName = displayName;
	}

	public abstract LinkedList<ScheduleEntry> getEntries();

	public ScheduleEntry getCurrentEntry() {
		ZonedDateTime dateTime = TheBell.getDateTime();
		for(ScheduleEntry entry : getEntries()) {
			if(!dateTime.isBefore(entry.getStartDateTime()) && !dateTime.isAfter(entry.getEndDateTime())) return entry;
		}
		return null;
	}

	public ScheduleEntry getNextEntry() {
		int index = getEntries().indexOf(getCurrentEntry()) + 1;
		if(index == getEntries().size()) return null; // day over

		return getEntries().get(index);
	}

	public MessageEmbed buildEmbed() {
		EmbedBuilder builder = new EmbedBuilder()
				.setTitle(this.displayName + " Schedule (" + DateTimeFormatter.ofPattern("MM/dd/yy").format(TheBell.getDateTime()) + ")")
				.setColor(Color.BLUE)
				.setFooter("Last Updated")
				.setTimestamp(Instant.now());

		for(ScheduleEntry entry : getEntries()) {
			if(entry instanceof PassingTimeEntry) continue;
			builder.addField(entry.getDisplayName(), entry.getStartTime() + " - " + entry.getEndTime(), false);
		}

		return builder.build();
	}

	public static MessageEmbed buildNullEmbed() {
		return new EmbedBuilder()
				.setTitle("No Schedule")
				.setColor(Color.RED)
				.setDescription("There is no school today, therefore, there is no schedule to follow!")
				.setFooter("Last Updated")
				.setTimestamp(Instant.now())
				.build();
	}

	public String getDisplayName() {
		return displayName;
	}

}
