/*
 * Copyright (c) 2022 Jack Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package me.jackbarter.ii.bell.schedules;

import me.jackbarter.ii.bell.Schedule;
import me.jackbarter.ii.bell.ScheduleEntry;
import me.jackbarter.ii.bell.entries.PassingTimeEntry;
import me.jackbarter.ii.bell.entries.PeriodEntry;

import java.util.Arrays;
import java.util.LinkedList;

public class StartOfYearSched extends Schedule {

	public StartOfYearSched() {
		super("Start of Year");
	}

	@Override
	public LinkedList<ScheduleEntry> getEntries() {
		return new LinkedList<>(Arrays.asList(
				new PassingTimeEntry("Clear Corridor", "7:20 AM", "7:30 AM", ""),
				new PeriodEntry("Period 1", "7:30 AM", "8:13 AM", ""),
				new PassingTimeEntry("Passing Time", "8:13 AM", "8:17 AM", ""),
				new PeriodEntry("Period 2C", "8:17 AM", "9:29 AM", ""),
				new PassingTimeEntry("Passing Time", "9:29 AM", "9:33 AM", ""),
				new PeriodEntry("Period 3", "9:33 AM", "10:16 AM", ""),
				new PassingTimeEntry("Passing Time", "10:16 AM", "10:20 AM", ""),
				new PeriodEntry("Period 4", "10:20 AM", "11:04 AM", ""),
				new PassingTimeEntry("Passing Time", "11:04 AM", "11:08 AM", ""),
				new PeriodEntry("Period 5", "11:08 AM", "11:52 AM", ""),
				new PassingTimeEntry("Passing Time", "11:52 AM", "11:56 AM", ""),
				new PeriodEntry("Period 6", "11:56 AM", "12:40 PM", ""),
				new PassingTimeEntry("Passing Time", "12:40 PM", "12:44 PM", ""),
				new PeriodEntry("Period 7", "12:44 PM", "1:28 PM", ""),
				new PassingTimeEntry("Passing Time", "1:28 PM", "1:32 PM", ""),
				new PeriodEntry("Period 8", "1:32 PM", "2:15 PM", "")
		));
	}
}
