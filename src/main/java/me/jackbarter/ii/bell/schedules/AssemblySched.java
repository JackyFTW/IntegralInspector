/*
 * Copyright (c) 2022 Jack Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package me.jackbarter.ii.bell.schedules;

import me.jackbarter.ii.bell.Schedule;
import me.jackbarter.ii.bell.ScheduleEntry;
import me.jackbarter.ii.bell.entries.PassingTimeEntry;
import me.jackbarter.ii.bell.entries.PeriodEntry;

import java.util.Arrays;
import java.util.LinkedList;

public class AssemblySched extends Schedule {

	public AssemblySched() {
		super("Assembly");
	}

	@Override
	public LinkedList<ScheduleEntry> getEntries() {
		return new LinkedList<>(Arrays.asList(
				new PassingTimeEntry("Clear Corridor", "7:20 AM", "7:30 AM", ""),
				new PeriodEntry("Period 1", "7:30 AM", "7:55 AM", ""),
				new PassingTimeEntry("Passing Time", "7:55 AM", "7:59 AM", ""),
				new PeriodEntry("Period 2 (11 & 12 Assembly)", "7:59 AM", "8:59 AM", ""),
				new PassingTimeEntry("Passing Time", "8:59 AM", "9:03 AM", ""),
				new PeriodEntry("Period 3 (9 & 10 Assembly)", "9:03 AM", "10:03 AM", ""),
				new PassingTimeEntry("Passing Time", "10:03 AM", "10:07 AM", ""),
				new PeriodEntry("Period 4", "10:07 AM", "10:37 AM", ""),
				new PassingTimeEntry("Passing Time", "10:37 AM", "10:41 AM", ""),
				new PeriodEntry("Period 5", "10:41 AM", "11:11 AM", ""),
				new PassingTimeEntry("Passing Time", "11:11 AM", "11:15 AM", ""),
				new PeriodEntry("Period 6", "11:15 AM", "11:45 AM", ""),
				new PassingTimeEntry("Passing Time", "11:45 AM", "11:49 AM", ""),
				new PeriodEntry("Period 7", "11:49 AM", "12:19 PM", ""),
				new PassingTimeEntry("Passing Time", "12:19 PM", "12:23 PM", ""),
				new PeriodEntry("Period 8", "12:23 PM", "12:48 PM", "")
		));
	}

}
