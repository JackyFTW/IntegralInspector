/*
 * Copyright (c) 2022 Jack Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package me.jackbarter.ii.bell.schedules;

import me.jackbarter.ii.bell.Schedule;
import me.jackbarter.ii.bell.ScheduleEntry;
import me.jackbarter.ii.bell.entries.PassingTimeEntry;
import me.jackbarter.ii.bell.entries.PeriodEntry;

import java.util.Arrays;
import java.util.LinkedList;

public class NormalSched extends Schedule {

	public NormalSched() {
		super("Normal");
	}

	@Override
	public LinkedList<ScheduleEntry> getEntries() {
		return new LinkedList<>(Arrays.asList(
				new PassingTimeEntry("Clear Corridor", "7:20 AM", "7:30 AM", ""),
				new PeriodEntry("Period 1", "7:30 AM", "8:17 AM", ""),
				new PassingTimeEntry("Passing Time", "8:17 AM", "8:21 AM", ""),
				new PeriodEntry("Period 2", "8:21 AM", "9:12 AM", ""),
				new PassingTimeEntry("Passing Time", "9:12 AM", "9:16 AM", ""),
				new PeriodEntry("Period 3", "9:16 AM", "10:03 AM", ""),
				new PassingTimeEntry("Passing Time", "10:03 AM", "10:07 AM", ""),
				new PeriodEntry("Period 4", "10:07 AM", "10:53 AM", ""),
				new PassingTimeEntry("Passing Time", "10:53 AM", "10:57 AM", ""),
				new PeriodEntry("Period 5", "10:57 AM", "11:43 AM", ""),
				new PassingTimeEntry("Passing Time", "11:43 AM", "11:47 AM", ""),
				new PeriodEntry("Period 6", "11:47 AM", "12:34 PM", ""),
				new PassingTimeEntry("Passing Time", "12:34 PM", "12:38 PM", ""),
				new PeriodEntry("Period 7", "12:38 PM", "1:24 PM", ""),
				new PassingTimeEntry("Passing Time", "1:24 PM", "1:28 PM", ""),
				new PeriodEntry("Period 8", "1:28 PM", "2:15 PM", "")
		));
	}

}
