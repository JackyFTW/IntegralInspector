/*
 * Copyright (c) 2022 Jack Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package me.jackbarter.ii.bell.schedules;

import me.jackbarter.ii.bell.Schedule;
import me.jackbarter.ii.bell.ScheduleEntry;
import me.jackbarter.ii.bell.entries.PassingTimeEntry;
import me.jackbarter.ii.bell.entries.PeriodEntry;

import java.util.Arrays;
import java.util.LinkedList;

public class NormalWednesSched extends Schedule {

	public NormalWednesSched() {
		super("Normal Wednesday");
	}

	@Override
	public LinkedList<ScheduleEntry> getEntries() {
		return new LinkedList<>(Arrays.asList(
				new PassingTimeEntry("Clear Corridor", "7:20 AM", "7:30 AM", ""),
				new PeriodEntry("Period 1", "7:30 AM", "8:03 AM", ""),
				new PassingTimeEntry("Passing Time", "8:03 AM", "8:07 AM", ""),
				new PeriodEntry("Period 2", "8:07 AM", "9:04 AM", ""),
				new PassingTimeEntry("Passing Time", "9:04 AM", "9:08 AM", ""),
				new PeriodEntry("Period 3", "9:08 AM", "9:41 AM", ""),
				new PassingTimeEntry("Passing Time", "9:41 AM", "9:45 AM", ""),
				new PeriodEntry("Period 4", "9:45 AM", "10:18 AM", ""),
				new PassingTimeEntry("Passing Time", "10:18 AM", "10:22 AM", ""),
				new PeriodEntry("Period 5", "10:22 AM", "10:55 AM", ""),
				new PassingTimeEntry("Passing Time", "10:55 AM", "10:59 AM", ""),
				new PeriodEntry("Period 6", "10:59 AM", "11:32 AM", ""),
				new PassingTimeEntry("Passing Time", "11:32 AM", "11:36 AM", ""),
				new PeriodEntry("Period 7", "11:36 AM", "12:09 PM", ""),
				new PassingTimeEntry("Passing Time", "12:09 PM", "12:13 PM", ""),
				new PeriodEntry("Period 8", "12:13 PM", "12:48 PM", "")
		));
	}
}
